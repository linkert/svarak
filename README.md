# Svårak keyboard layout

Custom keyboard layout for printing using custom keyboard layout service on wasdkeyboards.com - first attempt at blending the best of (sv)Dvorak layout on ISO hardware with aspects of ANSI utilized.

Set in [Anka/Coder](https://github.com/fitojb/anka-coder-fonts).

Need to work on how to actually use a custom layout file. Have to do some reading on the subject.